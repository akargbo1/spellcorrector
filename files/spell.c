/* ... */

#include <stdio.h>

// for string functions
#include <string.h>
#include <stdlib.h>

#include "spell.h"

// definitions for hash set
#define TABLE_SIZE 1007
typedef struct HashNode
{
    char word[50];
    struct HashNode *next;
} HashNode;

typedef struct
{
    HashNode *table[TABLE_SIZE];
} HashTable;

void lev_words(List *list, char *word, int distance);
HashTable *new_hash_table();
void hash_table_add(HashTable *table, const char *word);
bool hash_table_contains(HashTable *table, const char *word);
HashTable *create_hash_table(List *list);
bool is_corrected(List *list, HashTable *table);

// see Assignment Task 1: Computing edit distance
void print_edit_distance(char *word1, char *word2)
{

    int word1Len = strlen(word1);
    int word2Len = strlen(word2);

    int input;
    int remove;
    int substitute;

    int levDist[word1Len + 1][word2Len + 1];

    // initialise row (word1) (equivalent so lev dist btn an empty sting and word1)
    for (int i = 0; i <= word1Len; i++)
    {
        levDist[i][0] = i;
    }

    // initialise column (word2)
    for (int j = 0; j <= word2Len; j++)
    {
        levDist[0][j] = j;
    }

    // initialise row (word1), start from 1, bc (1,1)
    for (int i = 1; i <= word1Len; i++)
    {
        for (int j = 1; j <= word2Len; j++)
        {
            if (word1[i - 1] == word2[j - 1])
            {
                levDist[i][j] = levDist[i - 1][j - 1];
            }
            else
            {
                // implement calculation for input, remove, substitute
                input = levDist[i - 1][j] + 1;
                remove = levDist[i][j - 1] + 1;
                substitute = levDist[i - 1][j - 1] + 1;

                // find min btn input, remove and substitute
                if (input <= remove && input <= substitute)
                {
                    levDist[i][j] = input;
                }
                else if (remove <= input && remove <= substitute)
                {
                    levDist[i][j] = remove;
                }
                else
                {
                    levDist[i][j] = substitute;
                }
            }
        }
    }

    printf("%d\n", levDist[word1Len][word2Len]);
}

// see Assignment Task 2: Enumerating all possible edits
void print_all_edits(char *word)
{
    int distance = 1;
    List *editsList = new_list();
    lev_words(editsList, word, distance);

    Node *word_current = editsList->head;

    while (word_current != NULL)
    {
        char *levWord = (char *)(word_current->data);
        printf("%s\n", levWord);
        word_current = word_current->next;
    }

    free_list(editsList);
}

// see Assignment Task 3: Spell checking
void print_checked(List *dictionary, List *document)
{
    HashTable *dict_table = create_hash_table(dictionary);

    // Iterate through each word in the document list
    Node *doc_current = document->head;
    while (doc_current != NULL)
    {
        char *word = (char *)(doc_current->data);

        // Check if the word exists in the dictionary hash table
        if (hash_table_contains(dict_table, word))
        {
            printf("%s\n", word);
        }
        else
        {
            printf("%s?\n", word);
        }

        doc_current = doc_current->next;
    }

    // Free memory used by the hash table
    free(dict_table);
}

// see Assignment Task 4: Spelling correction
void print_corrected(List *dictionary, List *document)
{

    HashTable *dict_table = create_hash_table(dictionary);

    // Iterate through each word in the document list
    Node *doc_current = document->head;
    while (doc_current != NULL)
    {
        char *word = (char *)(doc_current->data);

        // Check if the word exists in the dictionary hash table
        if (hash_table_contains(dict_table, word))
        {
            printf("%s\n", word);
        }
        else
        {

            List *levOne = new_list();
            lev_words(levOne, word, 1);
            if (is_corrected(levOne, dict_table))
            {

                // printf("%s (corrected)\n", word);
                free_list(levOne);
            }
            else
            {

                free_list(levOne);
                List *levTwo = new_list();
                lev_words(levTwo, word, 2);
                if (is_corrected(levTwo, dict_table))
                {
                    // printf("%s (corrected)\n", word);
                    free_list(levTwo);
                }
                else
                {

                    free_list(levTwo);
                    printf("%s?\n", word);
                }
            }
        }
        doc_current = doc_current->next;
    }

    // Free memory used by the hash table
    free(dict_table);
}

// Additional functions

// function to calcualte word with specific leviton distance

void lev_words(List *list, char *word, int distance)
{

    int wordLen = strlen(word);

    // Base case: If we have reached the desired Levenshtein distance
    if (distance == 0)
    {
        list_add_end(list, word);
    }
    else
    {

        // iterate through word and do all possible operations
        // add words to a list
        for (int i = 0; i <= wordLen; i++)
        {
            for (char c = 'a'; c <= 'z'; c++)
            {
                // substitution
                char *newSubWord = (char *)malloc((wordLen + 1) * sizeof(char));
                strcpy(newSubWord, word);
                newSubWord[i] = c;
                lev_words(list, newSubWord, (distance - 1));
                // free(newSubWord);

                // insertion
                char *newInWord = (char *)malloc((wordLen + 2) * sizeof(char));
                int n = 0;
                for (int j = 0; j <= (wordLen + 1); j++)
                {
                    if (j == i)
                    {
                        newInWord[j] = c;
                    }
                    else
                    {
                        newInWord[j] = word[n];
                        n++;
                    }
                }

                lev_words(list, newInWord, (distance - 1));
                // free(newInWord);

                // deleletion

                if (i < wordLen)
                {
                    char *newDelWord = (char *)malloc((wordLen + 1) * sizeof(char));
                    int x = 0;
                    for (int j = 0; j <= wordLen; j++)
                    {
                        if (j != i)
                        {
                            newDelWord[x] = word[j];
                            x++;
                        }
                    }

                    newDelWord[wordLen - 1] = '\0';
                    lev_words(list, newDelWord, (distance - 1));
                    // free(newDelWord);
                }
            }
        }
    }
}

// Hash function
unsigned int hash(const char *word)
{
    unsigned int hash = 0;
    while (*word)
    {
        hash = (hash * 31) + *word;
        word++;
    }
    return hash % TABLE_SIZE;
}

// New hash table function
HashTable *new_hash_table()
{
    HashTable *table = malloc(sizeof(HashTable));
    if (table == NULL)
    {
        perror("Memory allocation error");
        exit(1);
    }
    for (int i = 0; i < TABLE_SIZE; i++)
    {
        table->table[i] = NULL;
    }
    return table;
}

// Add a word to the hash table
void hash_table_add(HashTable *table, const char *word)
{
    unsigned int index = hash(word);
    HashNode *node = malloc(sizeof(HashNode));
    if (node == NULL)
    {
        perror("Memory allocation error");
        exit(1);
    }
    strcpy(node->word, word);
    node->next = table->table[index];
    table->table[index] = node;
}

/// Check if a word exists in the hash table
bool hash_table_contains(HashTable *table, const char *word)
{
    unsigned int index = hash(word);
    HashNode *node = table->table[index];
    while (node != NULL)
    {
        if (strcmp(node->word, word) == 0)
        {
            return true;
        }
        node = node->next;
    }

    return false;
}

// function to make list into has table
HashTable *create_hash_table(List *list)
{

    HashTable *list_table = new_hash_table();

    // Add list words to the hash table
    Node *elem_current = list->head;
    while (elem_current != NULL)
    {
        char *dict_word = (char *)(elem_current->data);
        hash_table_add(list_table, dict_word);
        elem_current = elem_current->next;
    }

    return list_table;

    // Free memory used by the hash table
    free(list_table);
}

// helper function to search if modified lev words for task four exixts
bool is_corrected(List *list, HashTable *table)
{

    // Iterate through each word in the  list
    Node *word_current = list->head;
    while (word_current != NULL)
    {
        char *word = (char *)(word_current->data);

        // Check if the word exists in the dictionary hash table
        if (hash_table_contains(table, word))
        {
            printf("%s\n", word);
            return true;
        }
        else
        {
            word_current = word_current->next;
        }
    }

    return false;
}
